package msifeed.mc.misca.crabs.fight;

public class BattleDefines {
    public static final int SECS_BEFORE_LEAVE_BATTLE = 10;
    public static final int SECS_TO_DEAL_DAMAGE = 2;
    public static final int NOTIFICATION_RADIUS = 15;
}
