package msifeed.mc.misca.things;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import msifeed.mc.misca.things.items.CustomFood;
import msifeed.mc.misca.things.items.CustomWeapon;
import msifeed.mc.misca.things.items.Miscellaneous;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class MiscaThings {
    public static final MiscaCreativeTab blocksTab = new MiscaCreativeTab("misca.blocks");
    public static final MiscaCreativeTab itemsTab = new MiscaCreativeTab("misca.items");

    public static CustomWeapon customWeapon;
    public static CustomFood customFood;
    public static Miscellaneous miscellaneous;

    public void onInit(FMLInitializationEvent event) {
        GameRegistry.registerTileEntity(RegularChest.ChestEntity.class, "misca.ariadna_chest");

        for (int i = 1; i <= 25; i++) {
            final String id_base = "misca_door_";
            RegularDoor door = new RegularDoor(id_base, i, Material.wood);
            GameRegistry.registerBlock(door, id_base + i);
            GameRegistry.registerItem(door.item, id_base + "item_" + i);
        }

        for (int i = 1; i <= 10; i++) {
            final String id_base = "misca_iron_door_";
            RegularDoor door = new RegularDoor(id_base, i, Material.iron);
            GameRegistry.registerBlock(door, id_base + i);
            GameRegistry.registerItem(door.item, id_base + "item_" + i);
        }

        for (int i = 1; i <= 85; i++) {
            String name_base = "misca_block_";
            RegularBlock block = new RegularBlock(name_base, i);
            block.setResistance(10);
            GameRegistry.registerBlock(block, name_base + i);
        }

        for (int i = 1; i <= 5; i++) {
            String name_base = "misca_block_ub_";
            final RegularBlock block = new RegularBlock(name_base, i);
            block.setBlockUnbreakable().setResistance(6000000);
            GameRegistry.registerBlock(block, name_base + i);

            registerSlabAndStairsFor(block, -1, 6000000);
        }

        for (int i = 1; i <= 30; i++) {
            String name_base = "misca_block_wool_";
            final RegularBlock block = new RegularBlock(name_base, i);
            block.setStepSound(Block.soundTypeCloth).setHardness(0.8F);
            GameRegistry.registerBlock(block, name_base + i);

            registerSlabAndStairsFor(block, 0.8f, 0);
        }

        for (int i = 1; i <= 16; i++) {
            String name_base = "misca_block_clay_";
            RegularBlock block = new RegularBlock(name_base, i);
            block.setStepSound(Block.soundTypeGravel).setHardness(0.6F);
            GameRegistry.registerBlock(block, name_base + i);

            registerSlabAndStairsFor(block, 0.6f, 0);
        }

        for (int i = 1; i <= 30; i++) {
            String name_base = "misca_block_plank_";
            RegularBlock block = new RegularBlock(name_base, i);
            block.setHardness(2.0F).setResistance(5.0F).setStepSound(Block.soundTypeWood);
            GameRegistry.registerBlock(block, name_base + i);

            registerSlabAndStairsFor(block, 2, 5);
        }

        for (int i = 1; i <= 50; i++) {
            GameRegistry.registerBlock(new RegularPillar(i), RegularPillar.NAME_BASE + i);
        }

        for (int i = 1; i <= 50; i++) {
            String name_base = "misca_barrel_";
            GameRegistry.registerBlock(new RegularBarrel(name_base, i), name_base + i);
        }

        for (int i = 1; i <= 4; i++) {
            String name_base = "misca_barrel_ub_";
            RegularBarrel block = new RegularBarrel(name_base, i);
            block.setBlockUnbreakable().setResistance(6000000.0F);
            GameRegistry.registerBlock(block, name_base + i);
        }

        for (int i = 1; i <= 10; i++) {
            String name_base = "misca_torch_";
            GameRegistry.registerBlock(new RegularTorch(name_base, i).setLightLevel(0.9375F), name_base + i);
        }

        for (int i = 1; i <= 10; i++) {
            String name_base = "misca_candle_";
            GameRegistry.registerBlock(new RegularTorch(name_base, i).setLightLevel(0.5F), name_base + i);
        }

        for (int i = 1; i <= 10; i++) {
            GameRegistry.registerBlock(new RegularPane(i), RegularPane.NAME_BASE + i);
        }

        for (int i = 1; i <= 25; i++) {
            GameRegistry.registerBlock(new RegularCarpet(i), RegularCarpet.NAME_BASE + i);
        }

        for (int i = 1; i <= 5; i++) {
            GameRegistry.registerBlock(new RegularBrewingStand(i), RegularBrewingStand.NAME_BASE + i);
        }

        for (int i = 1; i <= 20; i++) {
            GameRegistry.registerBlock(new RegularChest(i), RegularChest.NAME_BASE + i);
        }

        for (int i = 1; i <= 15; i++) {
            String name_base = "misca_cross_";
            RegularCross block = new RegularCross(name_base, i);
            block.setHardness(4);
            GameRegistry.registerBlock(block, name_base + i);
        }

        for (int i = 1; i <= 15; i++) {
            String name_base = "misca_pad_";
            RegularPad block = new RegularPad(name_base, i);
            block.setHardness(4);
            GameRegistry.registerBlock(block, name_base + i);
        }

        for (int i = 1; i <= 15; i++) {
            final String id_base = "misca_bed_";
            RegularBed block = new RegularBed(id_base, i);
            GameRegistry.registerBlock(block, id_base + i);
            GameRegistry.registerItem(block.item, id_base + "item_" + i);
        }

        for (int i = 1; i <= 10; i++) {
            final String id_base = "misca_trapdoor_";
            RegularTrapdoor block = new RegularTrapdoor(id_base, i);
            GameRegistry.registerBlock(block, id_base + i);
        }

        customWeapon = new CustomWeapon(itemsTab);
        customFood = new CustomFood(itemsTab);
        miscellaneous = new Miscellaneous(itemsTab);

        GameRegistry.registerBlock(new TransparentBlock(), TransparentBlock.NAME_BASE);
        GameRegistry.registerBlock(new DripsBlock(), DripsBlock.NAME_BASE);
    }

    private void registerSlabAndStairsFor(Block block, float hardness, float resistance) {
        final RegularSlab halfSlab = new RegularSlab(block, false);
        final RegularSlab doubleSlab = new RegularSlab(block, true);
        halfSlab.setHardness(hardness);
        doubleSlab.setHardness(hardness);
        if (resistance > 0) {
            halfSlab.setResistance(resistance);
            doubleSlab.setResistance(resistance);
        }
//        final RegularSlab.Item slabItem = new RegularSlab.Item(halfSlab, doubleSlab);

        GameRegistry.registerBlock(halfSlab, RegularSlab.Item.class, halfSlab.getName(), halfSlab, doubleSlab);
        GameRegistry.registerBlock(doubleSlab, doubleSlab.getName());

        final RegularStairs stairs = new RegularStairs(block, 0);

        GameRegistry.registerBlock(stairs, stairs.getName());
    }
}
